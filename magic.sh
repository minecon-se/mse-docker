#!/bin/bash
case $1 in
	m1start )
		echo "Merp"
		;;
	m1stop )
		echo "Morp"
		;;
	m1rebuild )
		echo "BLERPBLORP"
		;;
	m2start )
		./launcher.sh 23423 blue 2 /root/mse-docker
		./launcher.sh 23433 red 2 /root/mse-docker
		./launcher.sh 23426 outside 2 /root/mse-docker
		;;
	m2stop )
		docker stop blue-2
		docker rm -f blue-2

		docker stop red-2
		docker rm -f red-2

		docker stop outside-2
		docker rm -f outside-2
		;;
	m2rebuild )
		;;
	m3start )

		./launcher.sh 23423 blue 3 /root/mse-docker
		./launcher.sh 23433 red 3 /root/mse-docker
		./launcher.sh 23426 outside 3 /root/mse-docker
		;;
	m3stop )
		docker stop blue-3
		docker rm -f blue-3

		docker stop red-3
		docker rm -f red-3

		docker stop outside-3
		docker rm -f outside-3
		;;
	m3rebuild )
		;;
	m4start )
		./launcher.sh 23423 blue 4 /root/mse-docker
		./launcher.sh 23433 red 4 /root/mse-docker
		./launcher.sh 23426 outside 4 /root/mse-docker
		;;
	m4stop )
		docker stop blue-4
		docker rm -f blue-4

		docker stop red-4
		docker rm -f red-4

		docker stop outside-4
		docker rm -f outside-4
		;;
	m4rebuild )
		;;
	m5start )
		./launcher.sh 23423 outside 5 /root/mse-docker
		./launcher.sh 23433 auditorium 1 /root/mse-docker
		./launcher.sh 23426 blue 1 /root/mse-docker
		;;
	m5stop )
		docker stop outside-5
		docker rm -f outside-5

		docker stop auditorium-1
		docker rm -f auditorium-1

		docker stop blue-1
		docker rm -f blue-1
		;;
	m5rebuild )
		;;
	m6start )
		./launcher.sh 23433 red 1 /root/mse-docker
		./launcher.sh 23426 outside 6 /root/mse-docker
		;;
	m6stop )
		docker stop red-1
		docker rm -f red-1
		docker stop outside-6
		docker rm -f outside-6
		;;
	m6rebuild )
		;;
	m7start )
		./launcher.sh 23426 outside 1 /root/mse-docker
		;;
	m7stop )
		docker stop outside-1
		docker rm -f outside-1
		;;
	m7rebuild )
		;;
esac
exit 0
