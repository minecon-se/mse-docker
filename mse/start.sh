if [ -z "$SPIGOTPORT" ]; then
  SPIGOTPORT=25565
fi
echo "Starting server on $SPIGOTPORT"
java -Xmx4G -Xincgc -XX:PermSize=64m -XX:MaxPermSize=256m -XX:ParallelGCThreads=2 -jar spigot.jar -p "$SPIGOTPORT"
