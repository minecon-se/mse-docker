## minecraft server

Minecraft instance runs in a docker container with the port and world files passed in. Launcher.sh is a wrapper script which handles calling docker with the correct flags.


### Running the server

```bash
./launcher.sh <port> <type> <id> [path to files]
```

port: Any free port (number)
type: One of: outside, blue, red, vip or auditorium
id: Consult table of server -> instance mappings
path to files: Path containing mse and mse1 (Defaults to /root)


### Updating the server

Change directory to the path containing mse and mse1, pull from git, then build the container.

```bash
cd <path> && git pull && docker build -t chamunks/outside:1 . 
```
