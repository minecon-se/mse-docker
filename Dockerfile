#############
## MSE2014 ##
#############
FROM crosbymichael/java
MAINTAINER Chamunks <Chamunks@gmail.com>

ADD		./mse1/plugins/ /plugins
ADD		./mse1/spigot.jar /spigot.jar
ADD		./mse1/eula.txt /eula.txt
ADD		./server.properties /server.properties
ADD		./mse1/spigot.yml /spigot.yml
ADD		./mse1/bukkit.yml /bukkit.yml
ADD		./mse1/whitelist.json /whitelist.json
ADD		./mse1/plugins /plugins
ADD		./mse1/.update-lock /.update-lock

## Worlds / CONFIGURE ME##

#ADD		./mse/outside1.yml /plugins/LilyPad-Connect/config.yml

#ADD		./mse1/worlds/outside /world
#ADD		http://hastebin.com/raw/eqalebaxup.vhdl /spigot.yml

#ADD		./mse1/worlds/auditorium /world
#ADD		http://hastebin.com/raw/odeturaziq.vhdl /spigot.yml

#ADD		./mse1/worlds/red /world
#ADD		http://hastebin.com/raw/aviguwetey.vhdl /spigot.yml
#ADD		./mse1/worlds/blue /world

###################
## Server Port   ##
###################
#ENV SPIGOT_PORT

ADD		./mse/start.sh /start.sh	
RUN		chmod +x start.sh
RUN		touch /.update-lock

ENTRYPOINT ["/bin/sh"]
CMD		["/start.sh"]
